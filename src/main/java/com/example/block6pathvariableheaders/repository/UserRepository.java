package com.example.block6pathvariableheaders.repository;

import com.example.block6pathvariableheaders.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long>  {
}
