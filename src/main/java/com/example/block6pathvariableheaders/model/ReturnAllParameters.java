package com.example.block6pathvariableheaders.model;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor
public class ReturnAllParameters {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String body;
    List<String> headers;
    List<String> requestParams;

    public ReturnAllParameters(String body, List<String> headers, List<String> requestParams) {
        this.body = body;
        this.headers = headers;
        this.requestParams = requestParams;
    }
}
