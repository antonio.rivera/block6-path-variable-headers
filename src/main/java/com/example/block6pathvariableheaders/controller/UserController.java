package com.example.block6pathvariableheaders.controller;


import com.example.block6pathvariableheaders.model.User;
import com.example.block6pathvariableheaders.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * Petición POST: mandando un objeto JSON en el body y recibiendo ese mismo objeto
     * JSON en la respuesta (en el body).
     */
    @PostMapping("/apartado1")
    ResponseEntity<User> returnSameUser(@RequestBody User user){
        return ResponseEntity.ok(userService.returnSameUser(user));
    }


    /**
     * Petición GET: mandando parámetros en el path (http://localhost:8080/user/{id}) y devolver
     * el mismo id mandado
     */
    @GetMapping("/user/{id}")
    ResponseEntity<Long> returnId(@PathVariable Long id){
        return ResponseEntity.ok(userService.returnID(id));
    }


    /**
     * Petición PUT: mandando Request Params (http://localhost:8080/post?var1=1&var2=2)
     * devolver un HashMap con los datos mandados . Por ejemplo: [ {var1: 1}, {var2: 2} ]
     */
    @PutMapping("/post")
    ResponseEntity<HashMap<String,Integer>> showParams(@RequestParam Integer var1, @RequestParam Integer var2){
        HashMap<String,Integer> result = userService.showParams(var1,var2);
        return ResponseEntity.ok(result);
    }



    /**
     * Petición GET: Mandar Header “h1” y “H2” a la URL http://localhost:8080/header. Cualquier
     * otro Header deberá ser ignorado (o no mostrado al menos)
     */
    @GetMapping("/header")
    ResponseEntity<HashMap<String,Integer>> showHeaders(@RequestHeader Integer var1, @RequestHeader Integer var2){
        HashMap<String,Integer> result = userService.showHeaders(var1,var2);
        return ResponseEntity.ok(result);
    }






}
