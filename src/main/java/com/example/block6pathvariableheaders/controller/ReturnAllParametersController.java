package com.example.block6pathvariableheaders.controller;


import com.example.block6pathvariableheaders.model.ReturnAllParameters;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ReturnAllParametersController {

    /**
     * Petición POST: Devolver todo los datos mandados: En la URL http://localhost:8080/all
     * devolver el body si es mandado, todos los Request Param y los headers en una objeto tipo:
     *
     *      String body;
     *      List<String> headers;
     *      List<String> requestParams;
     */
    @PostMapping("/all")
    ResponseEntity<ReturnAllParameters> devolverDatos(@RequestBody(required = false) String body,
                                                      @RequestHeader(required = false) String var1,
                                                      @RequestHeader(required = false) String var2,
                                                      @RequestParam(required = false) String var11,
                                                      @RequestParam(required = false) String var22){

        List<String> headers=new ArrayList<>();
        headers.add(var1);
        headers.add(var2);

        List<String> requestParams=new ArrayList<>();
        requestParams.add(var11);
        requestParams.add(var22);

        ReturnAllParameters returnAllParameters = new ReturnAllParameters(body,headers,requestParams);


        return ResponseEntity.ok(returnAllParameters);
    }
}
