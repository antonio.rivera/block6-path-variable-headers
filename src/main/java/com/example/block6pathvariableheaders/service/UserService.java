package com.example.block6pathvariableheaders.service;

import com.example.block6pathvariableheaders.model.User;

import java.util.HashMap;

public interface UserService {

    User returnSameUser(User user);

    Long returnID(Long id);

    HashMap<String,Integer> showParams(Integer var1, Integer var2);

    HashMap<String,Integer> showHeaders(Integer var1, Integer var2);

    // Crear el objeto



}
