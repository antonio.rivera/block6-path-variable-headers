package com.example.block6pathvariableheaders.service.impl;

import com.example.block6pathvariableheaders.model.User;
import com.example.block6pathvariableheaders.repository.UserRepository;
import com.example.block6pathvariableheaders.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User returnSameUser(User user) {
        return user;
    }

    @Override
    public Long returnID(Long id) {
        return id;
    }

    @Override
    public HashMap<String, Integer> showParams(Integer var1, Integer var2) {
        HashMap<String,Integer> result = new HashMap<String,Integer>();
        result.put("var1",var1);
        result.put("var2",var2);
        return result;
    }

    @Override
    public HashMap<String, Integer> showHeaders(Integer var1, Integer var2) {
        HashMap<String,Integer> result = new HashMap<String,Integer>();
        result.put("var1",var1);
        result.put("var2",var2);
        return result;
    }
}
